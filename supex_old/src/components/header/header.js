import React from 'react';
import { Container,Row,Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars,faSearch} from '@fortawesome/free-solid-svg-icons'
import './header.css';


export default function Header(){

	return (

		<div className="Header">
			<Container>
			<Row >
				<Col>
					<img src='img/logo.png' width='100px'/>
				</Col>
			</Row>
			<Row className="Header__menu">
				<Col className="Header__menu__bar">
					<FontAwesomeIcon icon={faBars} />
					<input type="text" placeholder="    Buscar el producto"/>
				</Col>
			</Row>
			</Container>
		</div>
		);
}