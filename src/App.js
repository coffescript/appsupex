import React from 'react';
import logo from './logo.svg';
import './App.css';
import Menu from './components/menu';
import Header from './components/header';
import Categories from './components/categories';
import Banner from './components/banner';
import Products from './components/products';
import {Fetch,useFetchLogin }from "./hooks/useFetch";
import { urlApiCategories,urlApiLogin } from "./utils/constants";
import "bootstrap/dist/css/bootstrap.min.css";




function App() {
  const loginData=[];
  let categories='';
  let  login=useFetchLogin(urlApiLogin,null);
  //console.log(login);
if(!login.loading && login.result){
 
   categories=login.result;
  
}

  return (
    <div className="App">
  <Header/>
    <Menu/>
    <Categories 
    categories={categories} />
   
     
    </div>
  );
}

export default App;
